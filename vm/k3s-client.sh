#!/bin/bash

# Server
export INSTALL_K3S_VERSION="v1.24.10+k3s1"
export INSTALL_K3S_SKIP_SELINUX_RPM=true
export INSTALL_K3S_SELINUX_WARN=true
export K3S_URL=https://<SERVERIP>:6443
export K3S_TOKEN="<TOKEN>"

# Install
curl -sfL https://get.k3s.io | sh -

# Start & Enable agent
systemctl enable --now k3s-agent
