#!/bin/bash
#title           :vm-manage.sh
#description     :This script automates VM deployments
#author          :pety.barczi@gmail.com
#date            :20231110
#version         :1.0
#=============================================================================================================

# Global Variables
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
vm_count=3
#vm_tenant="pbarczi"
vm_tenant=$2
vm_template="debian-12-template.qcow2"

################################################################################
################################################################################
# Main program                                                                 #
################################################################################
################################################################################

help()
{
   # Display Help
   echo "Usage:  vm-manage.sh [OPTIONS]"
   echo
   echo "Script for managing VM creation or deletion"
   echo
   echo "Options:"
   echo "  -c, --create <tenant-name>  Create VM"
   echo "  -d, --delete <tenant-name>  Delete VM"
   echo
   echo "Example:"
   echo "  vm-manage.sh -c your-tenant-name"
}

if [ $# -ne 2 ]; then
    echo -e "${RED}Number of optional parameters is incorrect! See the proper usage: ${NC}"
    help
    exit 1
fi

################
function vm_create() {
mkdir ${vm_tenant}
for ((i = 1; i <= vm_count; i++)); do
    # Your code here, using the loop variable $i
    echo "Processing VM $i"
    cp ${vm_template} ${vm_tenant}/${vm_tenant}-vm${i}.qcow2 
    cp .template.xml ${vm_tenant}/${vm_tenant}-vm${i}.xml
    sed -i "s/template-name/$vm_tenant-vm$i/g" ${vm_tenant}/${vm_tenant}-vm${i}.xml
    sed -i "s/template-img/$vm_tenant-vm$i/g" ${vm_tenant}/${vm_tenant}-vm${i}.xml
    sed -i "s/tenant/$vm_tenant/g" ${vm_tenant}/${vm_tenant}-vm${i}.xml
    sed -i "s/macid/$i/g" ${vm_tenant}/${vm_tenant}-vm${i}.xml
    qemu-img create -f qcow2 ${vm_tenant}/${vm_tenant}-vm${i}-disk1.qcow2 5G
    virsh define ${vm_tenant}/${vm_tenant}-vm${i}.xml
    virsh start ${vm_tenant}-vm${i}
done
}


function vm_delete() {
for ((i = 1; i <= vm_count; i++)); do
    echo "Processing VM $i"
    virsh destroy ${vm_tenant}-vm${i} --graceful
    virsh undefine ${vm_tenant}-vm${i}
    rm ${vm_tenant}/${vm_tenant}-vm${i}.qcow2 
    rm ${vm_tenant}/${vm_tenant}-vm${i}.xml
    rm ${vm_tenant}/${vm_tenant}-vm${i}-disk*
done
rmdir ${vm_tenant}
}



################################################################################
################################################################################
# Menu                                                                         #
################################################################################
################################################################################

case $1 in
  -c|--create)
    vm_create
    ;;
  -d|--delete)
    vm_delete
    ;;
  *)
    help
    exit 1
    ;;
esac
