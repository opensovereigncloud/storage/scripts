#!/bin/bash

# Server
export INSTALL_K3S_VERSION="v1.24.10+k3s1"
export INSTALL_K3S_SKIP_SELINUX_RPM=true
export INSTALL_K3S_SELINUX_WARN=true

# Install
curl -sfL https://get.k3s.io | sh -s server - --cluster-init --disable traefik --etcd-snapshot-schedule-cron "*/30 * * * *" --etcd-snapshot-retention "10"

# Generate token
#k3s token create

# Display Token
cat /var/lib/rancher/k3s/server/token

# kubeconfig
mkdir -p /root/.kube
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
cp -i /etc/rancher/k3s/k3s.yaml /root/.kube/config
chmod +r /etc/rancher/k3s/k3s.yaml
echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >> /etc/bashrc
