#!/bin/bash
# https://gitlab.com/pety-linux/k8s/flux/flux-sample

test -f /usr/local/bin/flux && {
        echo "Flux already installed!"
        exit 1
}

## Variables
export FLUX_VERSION=2.1.2
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
export PATH=$PATH:/usr/local/bin

## Install flux cli
echo "Installing flux CLI ..."
curl -s https://fluxcd.io/install.sh | bash

# bash autocompletion
. <(flux completion bash)

